// import logo from './logo.svg';
import './App.css';
import MindRead from './MindRead';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <MindRead/>
    </div>
  );
}

export default App;
